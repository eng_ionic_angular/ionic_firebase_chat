import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { LoginPage } from '../pages/login/login';
import { ChatProvider } from '../providers/chat/chat';
import { FormsModule } from '../../node_modules/@angular/forms';
import { ComponentsModule } from '../components/components.module';

export const firebaseConfig = {
  apiKey: "AIzaSyCpEa60qprSPNxVG6G3Uen5xbuCJaW8spo",
  authDomain: "fire-chat-73ef2.firebaseapp.com",
  databaseURL: "https://fire-chat-73ef2.firebaseio.com",
  projectId: "fire-chat-73ef2",
  storageBucket: "fire-chat-73ef2.appspot.com",
  messagingSenderId: "831047519604"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthenticationProvider,
    ChatProvider
  ]
})
export class AppModule {}
