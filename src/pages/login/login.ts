import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authProvider: AuthenticationProvider,
    private app: App /** Classe que controla acões e comportamentos do app */
  ) {
  }

  login(username, password) {
    this.authProvider.login(username, password).then(user => {
      this.app.getRootNav().setRoot(HomePage);
    }).catch(error => {
      console.log(error);
    })
  }

  goToSignup() {
    this.navCtrl.push(SignupPage);
  }

}
